import 'package:flutter/material.dart';
class PageMakePace extends StatefulWidget {
  const PageMakePace({
    super.key,
    required this.friendName
  });

  final String friendName;

  @override
  State<PageMakePace> createState() => _PageMakePaceState();
}

class _PageMakePaceState extends State<PageMakePace> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('랜덤 얼굴 그리기'),
      ),
      body: Text(widget.friendName),

    );
  }

  Widget _buildbody(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            Container(
              child: Stack(
                children: [
                    Image.asset('assets/face_2.png'),
                    Image.asset('assets/eyes_2.png'),
                    Image.asset('assets/nose_1.png'),
                    Image.asset('assets/mouse_1.png'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
