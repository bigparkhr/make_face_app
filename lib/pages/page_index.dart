import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();

}

class _PageIndexState extends State<PageIndex> {

  String friendName = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('랜덤 얼굴 그리기'),
      ),
      body: _buildbody(context)
    );
  }

  Widget _buildbody(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Container( margin: EdgeInsets.only(top: 150),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                style: TextStyle(fontSize: 20),
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: '이름 입력' ,
                ),
              ),
              Column(
                children: [
                  ElevatedButton(onPressed: () {},
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size.fromHeight(50),
                    ),
                    child: Text ("시작"),
                  )
                ],
              ),
              Column(
                children: [
                  Image.asset('assets/app1.png')
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
